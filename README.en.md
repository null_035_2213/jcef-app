# jcef-app

####Introduction

A Chrome browser application embedded in Java call CEF framework

####Software architecture

The software is Maven + JDK1.8_ sixty-four

####Installation tutorial

1. Use idea to import project

2. Add the files in the DLL folder (project structure - > Global libraries)

3. Right click to run chromeframe.java

####Instructions for use

1. DLL and JDK should be 64 bit or 32 bit

2. Please visit https://bitbucket.org/chromiumembedded/java-cef/get/62432af095e8.zip

3. JCEF local DLL dependency https://share.weiyun.com/ghuwIh2W