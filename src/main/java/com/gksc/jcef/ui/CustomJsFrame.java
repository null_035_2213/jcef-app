package com.gksc.jcef.ui;

import com.gksc.jcef.ChromeFrame;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.browser.CefMessageRouter;
import org.cef.callback.CefQueryCallback;
import org.cef.handler.CefMessageRouterHandler;

import java.io.File;

/**
 * @className: JsFrame
 * @description: JavaScript与后端交互页面
 * @author: Liao.yx
 * @email: 1229053515@qq.com
 * @create: 2021年05月28日 09时07分
 * @copyRight: 2020 liaoyongxiang All rights reserved.
 **/
public class CustomJsFrame extends ChromeFrame {

    public CustomJsFrame(String appName, String icon, int width, int height, int posX, int posY, String startURL, String logPath, boolean useOSR,
                         boolean isTransparent, boolean isShowAddr, boolean isCloseExit, boolean isChangeSize, String routerName, String cancelName) {
        super(appName, icon, width, height, posX, posY, startURL, logPath, useOSR, isTransparent, isShowAddr, isCloseExit, isChangeSize);
        jsActive(this.getClient(), routerName, cancelName);
    }

    public CustomJsFrame(String startURL, boolean useOSR, boolean isTransparent, String routerName, String cancelName) {
        super(startURL, useOSR, isTransparent);
        jsActive(this.getClient(), routerName, cancelName);
    }

    /**
     * 添加js交互
     * html页面可使用 window.routerName({}) 和 window.cancelName({}) 来调用此方法
     *
     * @param client     cef客户端
     * @param routerName 路由名称
     * @param cancelName 取消方法
     */
    private void jsActive(CefClient client, String routerName, String cancelName) {
        CefMessageRouter.CefMessageRouterConfig cmrc = new CefMessageRouter.CefMessageRouterConfig(routerName, cancelName);
        //创建查询路由
        CefMessageRouter cmr = CefMessageRouter.create(cmrc);
        cmr.addHandler(new CefMessageRouterHandler() {

            @Override
            public void setNativeRef(String str, long val) {
                System.out.println(str + "  " + val);
            }

            @Override
            public long getNativeRef(String str) {
                System.out.println(str);
                return 0;
            }

            @Override
            public void onQueryCanceled(CefBrowser browser, CefFrame frame, long query_id) {
                System.out.println("取消查询:" + query_id);
            }

            @Override
            public boolean onQuery(CefBrowser browser, CefFrame frame, long query_id, String request, boolean persistent, CefQueryCallback callback) {
                System.out.println("request:" + request + "\nquery_id:" + query_id + "\npersistent:" + persistent);

                callback.success("Java后台执行......");
                return true;
            }
        }, true);
        client.addMessageRouter(cmr);
    }

    public static void main(String[] args) {
        if (!CefApp.startup()) {
            System.out.println("Startup initialization failed!");
            return;
        }

        String url = (CustomJsFrame.class.getResource("/").getPath()).replaceAll("%20", " ").substring(1).replace("/", "\\");
        url = url + "page" + File.separator + "test.html";
        new CustomJsFrame(url, false, false, "test", "testCancel");
    }
}
