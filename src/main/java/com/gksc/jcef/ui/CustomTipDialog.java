package com.gksc.jcef.ui;

import com.gksc.jcef.util.Tool;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.Callable;

/**
 * @className: CustomTipDialog
 * @description: 自定义提示弹框
 * @author: Liao.yx
 * @email: 1229053515@qq.com
 * @create: 2021年05月27日 17时09分
 * @copyRight: 2020 liaoyongxiang All rights reserved.
 **/
public class CustomTipDialog implements Callable<Integer> {
    /**
     * 父组件
     */
    private Frame parent;
    /**
     * 错误消息
     */
    private String errorMsg;
    /**
     * 提示框标题
     */
    private String title;
    /**
     * 请求路径
     */
    private String requestUrl;
    /**
     * 消息提示类型
     *
     * @see javax.swing.JOptionPane
     */
    private int tipType;
    /**
     * 操作选项
     *
     * @see javax.swing.JOptionPane
     */
    private int option;

    public CustomTipDialog(Frame parent, String errorMsg, String title, String requestUrl, int tipType, int option) {
        this.parent = parent;
        this.errorMsg = errorMsg;
        this.title = title;
        this.requestUrl = requestUrl;
        this.tipType = tipType;
        this.option = option;
    }

    @Override
    public Integer call() {
        if (!Tool.isEmpty(requestUrl)) {
            errorMsg = "请求" + requestUrl + "结果为：" + errorMsg;
        }
        return JOptionPane.showConfirmDialog(parent, errorMsg, title, option, tipType);
    }


}
