package com.gksc.jcef.util;

/**
 * @className: Tool
 * @description: 工具类
 * @author: Liao.yx
 * @email: 1229053515@qq.com
 * @create: 2021年05月27日 17时37分
 * @copyRight: 2020 liaoyongxiang All rights reserved.
 **/
public class Tool {

    /**
     * 判断字符串是否为空
     *
     * @param arg 待比较内容
     * @return
     */
    public static boolean isEmpty(String arg) {
        if (arg == null || arg.length() == 0) {
            return true;
        }
        return false;
    }
}
