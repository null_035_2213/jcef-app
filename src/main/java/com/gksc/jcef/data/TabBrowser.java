package com.gksc.jcef.data;

import org.cef.browser.CefBrowser;

import javax.swing.*;

/**
 * @className: TabBrowser
 * @description: tab页面信息
 * @author: Liao.yx
 * @email: 1229053515@qq.com
 * @create: 2021年05月28日 10时15分
 * @copyRight: 2020 liaoyongxiang All rights reserved.
 **/
public class TabBrowser {
    /**
     * 页面唯一索引
     */
    private int index;
    /**
     * 浏览器对象
     */
    private CefBrowser browser;
    /**
     * 标题
     */
    private JLabel title;
    /**
     * 定制栏控件
     */
    private JTextField address;
    /**
     * 浏览器是否获取焦点
     */
    private boolean browserFocus = true;

    public TabBrowser(int index, CefBrowser browser, JLabel title, JTextField address, boolean browserFocus) {
        this.index = index;
        this.browser = browser;
        this.title = title;
        this.address = address;
        this.browserFocus = browserFocus;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public CefBrowser getBrowser() {
        return browser;
    }

    public void setBrowser(CefBrowser browser) {
        this.browser = browser;
    }

    public JLabel getTitle() {
        return title;
    }

    public void setTitle(JLabel title) {
        this.title = title;
    }

    public JTextField getAddress() {
        return address;
    }

    public void setAddress(JTextField address) {
        this.address = address;
    }

    public boolean isBrowserFocus() {
        return browserFocus;
    }

    public void setBrowserFocus(boolean browserFocus) {
        this.browserFocus = browserFocus;
    }
}
