package com.gksc.jcef.constant;

/**
 * @className: BusinessConstant
 * @description: 业务常量类
 * @author: Liao.yx
 * @email: 1229053515@qq.com
 * @create: 2021年05月28日 11时23分
 * @copyRight: 2020 liaoyongxiang All rights reserved.
 **/
public class BusinessConstant {
    /**
     * frame默认宽高
     */
    public static final int width = 1000;
    public static final int height = 600;
}