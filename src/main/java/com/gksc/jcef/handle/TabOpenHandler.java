package com.gksc.jcef.handle;

import com.gksc.jcef.ChromeTabFrame;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.handler.CefDisplayHandlerAdapter;

/**
 * @className: TabOpenHandler
 * @description: tab页面展示处理逻辑
 * @author: Liao.yx
 * @email: 1229053515@qq.com
 * @create: 2021年05月28日 10时22分
 * @copyRight: 2020 liaoyongxiang All rights reserved.
 **/
public class TabOpenHandler extends CefDisplayHandlerAdapter {
    private String title;
    private ChromeTabFrame tabFrame;

    public TabOpenHandler(String title, ChromeTabFrame tabFrame) {
        this.title = title;
        this.tabFrame = tabFrame;
    }

    @Override
    public void onAddressChange(CefBrowser browser, CefFrame frame, String url) {
        tabFrame.updateTabTitle(title, url, browser);
    }
}
