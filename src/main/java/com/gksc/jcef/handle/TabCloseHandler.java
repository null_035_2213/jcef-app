package com.gksc.jcef.handle;

import com.gksc.jcef.ChromeTabFrame;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @className: TabCloseHandler
 * @description: tab页面关闭处理逻辑
 * @author: Liao.yx
 * @email: 1229053515@qq.com
 * @create: 2021年05月28日 10时32分
 * @copyRight: 2020 liaoyongxiang All rights reserved.
 **/
public class TabCloseHandler implements MouseListener {
    private int index;
    private ChromeTabFrame frame;

    public TabCloseHandler(int index, ChromeTabFrame frame) {
        this.index = index;
        this.frame = frame;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("点击关闭Tab页");
        frame.removeTab(index);
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
