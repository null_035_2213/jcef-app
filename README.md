# jcef-app

#### 介绍
java调用cef框架内嵌一个Chrome浏览器应用程序

#### 软件架构
软件采用maven + jdk1.8_64


#### 安装教程
1. 使用IDEA导入项目
2. 添加项目外部依赖dll文件夹中的文件（Project Structure -> Global Libraries）
3. 右键运行ChromeFrame.java

#### 使用说明
1. dll与jdk应同为64位或32位
2. jcef源码下载请访问https://bitbucket.org/chromiumembedded/java-cef/get/62432af095e8.zip
3. jcef本地dll依赖https://share.weiyun.com/ghuwIh2W
4. 运行时可以直接将dll文件复制到jdk/bin目录下，也可以携带参数-Djava.library.path=E:\mywork\work5\jcef-win64\dll\win64指定dll路径
